import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import * as k8s from "@pulumi/kubernetes";

const gcpConfig = new pulumi.Config("gcp");
const gcpProject = gcpConfig.require("project");
const gcpZone = gcpConfig.require("zone");

const name = "global";

const k8sVersion = "latest";

// Create a GKE cluster
const cluster = new gcp.container.Cluster(name, {
    initialNodeCount: 1,
    minMasterVersion: k8sVersion,
    nodeVersion: k8sVersion,
    nodeConfig: {
        machineType: "g1-small",
        oauthScopes: [
            "https://www.googleapis.com/auth/compute",
            "https://www.googleapis.com/auth/devstorage.read_only",
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring"
        ],
    },
}, { additionalSecretOutputs: ["masterAuth"] });

// Export the Cluster name
export const clusterName = cluster.name;

// Manufacture a GKE-style kubeconfig. Note that this is slightly "different"
// because of the way GKE requires gcloud to be in the picture for cluster
// authentication (rather than using the client cert/key directly).
export const kubeconfig = pulumi.
    all([cluster.name, cluster.endpoint, cluster.masterAuth]).
    apply(([name, endpoint, masterAuth]) => {
        const context = `${gcpProject}_${gcpZone}_${name}`;
        return `apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${masterAuth.clusterCaCertificate}
    server: https://${endpoint}
  name: ${context}
contexts:
- context:
    cluster: ${context}
    user: ${context}
  name: ${context}
current-context: ${context}
kind: Config
preferences: {}
users:
- name: ${context}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{.credential.token_expiry}'
        token-key: '{.credential.access_token}'
      name: gcp
`;
    });

// Create a Kubernetes provider instance that uses our cluster from above.
const clusterProvider = new k8s.Provider(name, {
    kubeconfig: kubeconfig,
});

// Create a Kubernetes Namespace.
export const namespace = new k8s.core.v1.Namespace(name, {
    metadata: {
        name: "rocore",
    },
}, { provider: clusterProvider });

// Create a service account to use for acceptance testing.
export const acceptanceTestSvcAccount = new gcp.serviceAccount.Account(name + "-acc-test-svc-acct", {
    accountId: "acceptance-test",
    displayName: "Acceptance Test Service Account",
});


// Create a service account for the pod under test during acceptance testing.
export const testSvcAccount = new gcp.serviceAccount.Account(name + "-test-svc-acct", {
    accountId: "test-svc",
    displayName: "Test Service - Service Account",
});

// Create a service account to use for production.
export const prodSvcAccount = new gcp.serviceAccount.Account(name + "-prod-svc-acct", {
    accountId: "production",
    displayName: "Production Service Account",
});
