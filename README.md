# global-infra

This repository contains longer lived infrastructure.

## Directories

- infrastructure: Long lived infrastructure
- base-image: base Dockerfile image used for deploying Pulumi infra
